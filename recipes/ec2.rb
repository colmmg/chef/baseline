[
  "jq",
  "git",
  "python3"
].each do |package|
  package "#{package}" do
    action :install
  end
end

[
  {:name => "etc/asg-info.sh", :owner => "root", :group => "root", :mode => "755"},
  {:name => "etc/ec2-info.sh", :owner => "root", :group => "root", :mode => "755"},
  {:name => "etc/ec2-tags.sh", :owner => "root", :group => "root", :mode => "755"},
].each do |file|
  cookbook_file "/#{file[:name]}" do
    source "#{file[:name]}"
    owner "#{file[:owner]}"
    group "#{file[:group]}"
    mode "#{file[:mode]}"
    action :create
  end
end

[
  "boto3"
].each do |python_package|
  execute "install #{python_package}" do
    command "pip3 install #{python_package}"
    not_if { `pip3 show #{python_package}`.match(/#{python_package}/) }
  end
end

execute "update amazon-ssm-agent" do
  command "yum update -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm"
end

include_recipe "baseline::inspector"
include_recipe "baseline::cloudwatch"
include_recipe "baseline::docker"
