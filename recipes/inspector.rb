remote_file '/tmp/install' do
  source 'https://inspector-agent.amazonaws.com/linux/latest/install'
  owner 'root'
  group 'root'
  mode '0700'
  action :create
  only_if { `rpm -q AwsAgent`.match(/is not installed/) }
end

execute 'install aws inspector agent' do
  command 'bash /tmp/install'
  only_if { `rpm -q AwsAgent`.match(/is not installed/) }
end

file '/tmp/install' do
  action :delete
end
