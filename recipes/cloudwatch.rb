remote_file '/tmp/amazon-cloudwatch-agent.rpm' do
  source 'https://s3.amazonaws.com/amazoncloudwatch-agent/amazon_linux/amd64/latest/amazon-cloudwatch-agent.rpm'
  owner 'root'
  group 'root'
  mode '0700'
  action :create
  only_if { `rpm -q amazon-cloudwatch-agent`.match(/is not installed/) }
end

execute 'install aws cloudwatch agent' do
  command 'rpm -U /tmp/amazon-cloudwatch-agent.rpm'
  only_if { `rpm -q amazon-cloudwatch-agent`.match(/is not installed/) }
end

file '/tmp/amazon-cloudwatch-agent.rpm' do
  action :delete
end
