execute 'install epel for Amazon Linux' do
  command 'amazon-linux-extras install -y epel'
end

execute "yum update" do
  command "yum update -y"
  live_stream true
end

package "tree" do
  action :install
end

include_recipe "os-hardening"
include_recipe "baseline::ec2" unless node['colmmg']['docker']
