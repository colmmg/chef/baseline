execute 'install docker' do
  command 'amazon-linux-extras install -y docker'
end

group 'docker' do
  append true
  members ['ec2-user']
  action :modify
end

service 'docker' do
  action [:disable]
end
