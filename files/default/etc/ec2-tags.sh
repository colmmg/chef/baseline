#!/bin/bash

sourceEc2InfoIfRequired() {
  if [ "${INSTANCE_ID}" ]; then
    return 0
  fi
  if [ ! -e /etc/ec2-info.sh ]; then
    printf "No /etc/ec2-info.sh found!\n"
    exit 1
  fi
  source /etc/ec2-info.sh
}

getInstanceDescription() {
  DESCRIBE_INSTANCES_OUTPUT=`mktemp`
  aws ec2 describe-instances \
    --instance-ids ${INSTANCE_ID} \
    --region $AWS_REGION \
    --output json > $DESCRIBE_INSTANCES_OUTPUT
}

getNameTag() {
  export EC2_TAGS_NAME=$(cat $DESCRIBE_INSTANCES_OUTPUT | jq '.Reservations[0].Instances[0].Tags[] | select(.Key == "Name") |.Value' | tr -d '"')
  if [ ! "$EC2_TAGS_NAME" ]; then
    printf "Could not find the Name tag value for instance ${INSTANCE_ID}!\n"
  fi
}

cleanupDescribeInstancesOutput() {
  rm -f $DESCRIBE_INSTANCES_OUTPUT
}

sourceEc2InfoIfRequired
getInstanceDescription
getNameTag
cleanupDescribeInstancesOutput
