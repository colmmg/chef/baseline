#!/bin/bash

exportInstanceId() {
  export INSTANCE_ID="$(curl -sq http://169.254.169.254/latest/meta-data/instance-id)"
  if [ ! "$INSTANCE_ID" ]; then
    printf "No INSTANCE_ID detected!\n"
    exit 1
  fi
}

exportInstanceIp() {
  export INSTANCE_IP="`hostname -I |xargs`"
}

exportAwsRegion() {
  local az=$(curl -sq http://169.254.169.254/latest/meta-data/placement/availability-zone/)
  export AWS_REGION="$(echo $az |sed 's/.$//')"
  if [ ! "$AWS_REGION" ]; then
    printf "No AWS_REGION detected!\n"
    exit 1
  fi
}

exportAwsAz() {
  local az=$(curl -sq http://169.254.169.254/latest/meta-data/placement/availability-zone/)
  export AWS_AZ=$(echo $az | sed "s,$AWS_REGION,,g")
  if [ ! "$AWS_AZ" ]; then
    printf "No AWS_AZ detected!\n"
    exit 1
  fi
}

exportAwsAccountId() {
  export AWS_ACCOUNT_ID=$(curl -sq http://169.254.169.254/latest/dynamic/instance-identity/document | grep -oP '(?<="accountId" : ")[^"]*(?=")')
  if [ ! "$AWS_ACCOUNT_ID" ]; then
    printf "No AWS_ACCOUNT_ID detected!\n"
    exit 1
  fi
}

getMacId() {
  MAC_ID="$(curl -sq http://169.254.169.254/latest/meta-data/mac)"
  if [ ! "$MAC_ID" ]; then
    printf "Could not find mac from instance meta-data!\n"
    exit 1
  fi
}

exportAwsVpcId() {
  export AWS_VPC_ID="$(curl -sq http://169.254.169.254/latest/meta-data/network/interfaces/macs/$MAC_ID/vpc-id)"
  if [ ! "$AWS_VPC_ID" ]; then
    printf "Could not find vpc id from instance meta-data!\n"
    exit 1
  fi
}

exportAwsSubnetId() {
  export AWS_SUBNET_ID="$(curl -sq http://169.254.169.254/latest/meta-data/network/interfaces/macs/$MAC_ID/subnet-id)"
  if [ ! "$AWS_SUBNET_ID" ]; then
    printf "Could not find subnet id from instance meta-data!\n"
    exit 1
  fi
}

exportSshKeyPair() {
  export AWS_SSH_KEY_PAIR="$(curl -s http://169.254.169.254/latest/meta-data/public-keys/0/openssh-key |cut -d ' ' -f3)"
  if [ ! "$AWS_SSH_KEY_PAIR" ] ;then
    printf "Could not find key pair from instance meta-data!!\n"
    exit 1
  fi
}

exportEcrHost() {
  export REGISTRY_HOST="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com"
}

exportInstanceId
exportInstanceIp
exportAwsRegion
exportAwsAz
exportAwsAccountId
getMacId
exportAwsVpcId
exportAwsSubnetId
exportSshKeyPair
exportEcrHost
