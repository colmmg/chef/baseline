#!/bin/bash

sourceEc2InfoIfRequired() {
  if [ "${INSTANCE_ID}" ]; then
    return 0
  fi
  if [ ! -e /etc/ec2-info.sh ]; then
    printf "No /etc/ec2-info.sh found!\n"
    exit 1
  fi
  source /etc/ec2-info.sh
}

getAsgName() {
  ASG_NAME=$(cat /etc/asg-info.sh.cache)
  if [ "$ASG_NAME" ]; then
    return 0
  fi
  ASG_NAME=$(aws ec2 describe-instances \
             --instance-ids ${INSTANCE_ID} \
             --region $AWS_REGION \
             --query "Reservations[*].Instances[*].[Tags[?Key=='aws:autoscaling:groupName'].Value]" \
             --output text)
  if [ ! "$ASG_NAME" ]; then
    exit 1
  fi
  echo "$ASG_NAME" > /etc/asg-info.sh.cache
}

getAsgDescription() {
  DESCRIBE_ASG_OUTPUT=`mktemp`
  aws autoscaling describe-auto-scaling-groups \
    --auto-scaling-group-names ${ASG_NAME} \
    --region $AWS_REGION \
    --output json > $DESCRIBE_ASG_OUTPUT
}

getAsgLifecycleState() {
  export ASG_LIFECYCLE_STATE=$(cat $DESCRIBE_ASG_OUTPUT | \
                               jq --arg INSTANCE_ID "$INSTANCE_ID" '.AutoScalingGroups[0].Instances[] | select(.InstanceId == $INSTANCE_ID) |.LifecycleState' | \
                               tr -d '"')
}

getAsgHealthStatus() {
  export ASG_HEALTH_STATUS=$(cat $DESCRIBE_ASG_OUTPUT | \
                             jq --arg INSTANCE_ID "$INSTANCE_ID" '.AutoScalingGroups[0].Instances[] | select(.InstanceId == $INSTANCE_ID) |.HealthStatus' | \
                             tr -d '"')
}

getAsgTargetGroup() {
  export ASG_TARGET_GROUP=$(cat $DESCRIBE_ASG_OUTPUT | \
                            jq '.AutoScalingGroups[0].TargetGroupARNs[]' | \
                            tr -d '"')
}

getAsgBlueGreenStatus() {
  export ASG_BLUE_GREEN_STATUS=$(cat $DESCRIBE_ASG_OUTPUT | \
                                 jq '.AutoScalingGroups[0].Tags[] | select(.Key == "BlueGreenStatus") |.Value' | \
                                 tr -d '"')
}

cleanupDescribeAsgOutput() {
  rm -f $DESCRIBE_ASG_OUTPUT
}

getAsgInstanceInService() {
  if [[ "$ASG_LIFECYCLE_STATE" == "InService" ]] && [[ "$ASG_HEALTH_STATUS" == "Healthy" ]] && [[ "$ASG_TARGET_GROUP" ]]; then
    export ASG_INSTANCE_IN_SERVICE=true
  fi
}

sourceEc2InfoIfRequired
getAsgName
getAsgDescription
getAsgLifecycleState
getAsgHealthStatus
getAsgTargetGroup
getAsgBlueGreenStatus
cleanupDescribeAsgOutput
getAsgInstanceInService
